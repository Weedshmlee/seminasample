package com.weedscomm.designpattern

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.weedscomm.designpattern.behavioral.command.CommandPattern
import com.weedscomm.designpattern.behavioral.observer.ObserverPattern
import com.weedscomm.designpattern.behavioral.state.IsNotStatePattern
import com.weedscomm.designpattern.behavioral.state.StatePattern
import com.weedscomm.designpattern.create.builder.PizzaMaker
import com.weedscomm.designpattern.structural.decorator.DecoratorPattern
import com.weedscomm.designpattern.structural.proxy.ProxyPattern

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val maker = PizzaMaker()
        maker.run()

        val proxy = ProxyPattern();
        proxy.run()

        val decorator = DecoratorPattern()
        decorator.baristaMakeCoffee()

        val observer = ObserverPattern()
        observer.run()

        val command = CommandPattern()
        command.run()

        val state = StatePattern()
        state.run()

        val state2 = IsNotStatePattern()
        state2.run()
    }
}
