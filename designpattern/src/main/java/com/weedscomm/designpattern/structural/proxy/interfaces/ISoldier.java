package com.weedscomm.designpattern.structural.proxy.interfaces;

/**
 * Created by hmlee on 2018-02-26.
 */

public interface ISoldier {
    void working();
}
