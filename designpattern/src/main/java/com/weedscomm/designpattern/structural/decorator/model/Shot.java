package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class Shot extends Additive {
    public Shot(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public String getName() {
        if(coffee.getName().equals(" 커피")) {
            return " 에스프레소";
        }
        return coffee.getName() + " 샷 추가";
    }
}
