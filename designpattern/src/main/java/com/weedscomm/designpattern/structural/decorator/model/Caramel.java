package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class Caramel extends Additive {
    public Caramel(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public String getName() {
        return "캬라멜향추가 " + coffee.getName();
    }
}
