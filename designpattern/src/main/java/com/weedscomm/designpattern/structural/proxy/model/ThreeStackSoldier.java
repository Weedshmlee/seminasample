package com.weedscomm.designpattern.structural.proxy.model;

import android.util.Log;

/**
 * Created by hmlee on 2018-02-26.
 */

public class ThreeStackSoldier extends Soldier {
    private final String TAG = "상병";

    public ThreeStackSoldier() {
        // 상병은 이미 준비되어 있습니다.
        Log.v(TAG, "새로운 상병이 선택받았습니다");

    }

    @Override
    protected void listening() {
        working();
    }

    @Override
    public void working() {
        Log.v(TAG, "상병은 노력을 합니다");
    }
}
