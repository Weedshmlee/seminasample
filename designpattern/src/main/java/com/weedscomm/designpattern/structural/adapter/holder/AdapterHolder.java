package com.weedscomm.designpattern.structural.adapter.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.weedscomm.designpattern.R;

/**
 * Created by hmlee on 2018-02-26.
 */

public class AdapterHolder extends RecyclerView.ViewHolder {
    private TextView textView;

    public AdapterHolder(View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.textView);
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}
