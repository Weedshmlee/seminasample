package com.weedscomm.designpattern.structural.proxy.model;

import android.util.Log;

import com.weedscomm.designpattern.structural.proxy.interfaces.ICaptain;

/**
 * Created by hmlee on 2018-02-26.
 */

public class OnCaller implements ICaptain {
    private final String TAG = "당직사관";

    private Soldier soldier1;
    private Soldier soldier2;
    private Soldier soldier3;
    private Soldier soldier4;

    public OnCaller() {
        // 당직사관은 중대장이 한 말을 그대로 수행합니다.
        // 수행 완료까지 세부적인 처리는 Proxy 에서 처리한다
        // 하지만 결과값을 조작하거나 변경하면 안된다
    }

    @Override
    public void listen(String str) {
        Log.v(TAG, "당직사관은 \"" + str + "\" 라는 말을 들었다");
    }

    @Override
    public void order(String str) {
        Log.v(TAG, "당직사관은 \"" + str + "\" 라는 말을 듣고 병사에게 명령합니다");

        if(null == soldier1) {
            soldier1 = new LastStackSoldier();
        }

        if(null == soldier2) {
            soldier2 = new ThreeStackSoldier();
        }

        if(null == soldier3) {
            soldier3 = new TwoStackSoldier();
        }

        if(null == soldier4) {
            soldier4 = new OneStackSoldier();
        }

        soldier1.working();
        soldier2.working();
        soldier3.working();
        soldier4.working();
    }
}
