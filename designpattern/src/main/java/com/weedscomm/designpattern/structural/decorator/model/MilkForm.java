package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class MilkForm extends Additive {
    public MilkForm(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public String getName() {
        return "우유거품추가 " + coffee.getName();
    }
}
