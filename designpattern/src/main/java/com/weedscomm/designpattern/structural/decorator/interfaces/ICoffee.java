package com.weedscomm.designpattern.structural.decorator.interfaces;

/**
 * Created by leewp on 18-02-26.
 */

public interface ICoffee {
    String getName();
}
