package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public abstract class Additive implements ICoffee {
    protected ICoffee coffee;

    public Additive(ICoffee coffee) {
        this.coffee = coffee;
    }

    public abstract String getName();
}
