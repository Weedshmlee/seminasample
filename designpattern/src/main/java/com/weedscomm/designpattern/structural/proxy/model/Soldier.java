package com.weedscomm.designpattern.structural.proxy.model;

import com.weedscomm.designpattern.structural.proxy.interfaces.ISoldier;

/**
 * Created by hmlee on 2018-02-26.
 */

public abstract class Soldier implements  ISoldier {
    protected abstract void listening();
}
