package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class Vanilla extends Additive {
    public Vanilla(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public String getName() {
        return "바닐라추가" + coffee.getName();
    }
}
