package com.weedscomm.designpattern.structural.decorator;

import android.util.Log;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;
import com.weedscomm.designpattern.structural.decorator.model.Caramel;
import com.weedscomm.designpattern.structural.decorator.model.Cup;
import com.weedscomm.designpattern.structural.decorator.model.Milk;
import com.weedscomm.designpattern.structural.decorator.model.MilkForm;
import com.weedscomm.designpattern.structural.decorator.model.Shot;
import com.weedscomm.designpattern.structural.decorator.model.Water;

/**
 * Created by leewp on 18-02-26.
 */

public class DecoratorPattern {
    private final String TAG = "Barista";

    public void baristaMakeCoffee() {
        ICoffee americano = new Water(new Shot(new Cup()));

        ICoffee espressoAddShot = new Shot(new Shot((new Cup())));

        ICoffee caramelMacchiato = new MilkForm(new Milk(new Caramel(new Water(new Shot(new Cup())))));

        Log.v(TAG, "아메리카노 : " + americano.getName() + "\n" +
                "에스프레소 샷추가 : " + espressoAddShot.getName() + "\n" +
                "캬라멜 마끼야또 : " + caramelMacchiato.getName());
    }
}
