package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class WhippedCream extends Additive {
    public WhippedCream(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public String getName() {
        return "휘핑크림추가 " + coffee.getName();
    }
}
