package com.weedscomm.designpattern.structural.proxy.model;

import android.util.Log;

/**
 * Created by hmlee on 2018-02-26.
 */

public class OneStackSoldier extends Soldier {
    private final String TAG = "이병";

    public OneStackSoldier() {
        // 이등병은 준비에 매우 오랜 시간이 걸립니다
        Log.v(TAG, "새로운 이병이 선택받았습니다");
    }

    @Override
    protected void listening() {
        working();
    }

    @Override
    public void working() {
        Log.v(TAG, "이병은 선임이 갈군다고 마음의편지를 씁니다");
    }
}
