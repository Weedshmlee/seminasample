package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class Water extends Additive {
    public Water(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public String getName() {
        if(coffee.getName().equals(" 에스프레소")) {
            return " 아메리카노";
        }
        return coffee.getName() + " 물 추가";
    }
}
