package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class Milk extends Additive {
    public Milk(ICoffee coffee) {
        super(coffee);
    }

    @Override
    public String getName() {
        return  "우유추가 " + coffee.getName();
    }
}
