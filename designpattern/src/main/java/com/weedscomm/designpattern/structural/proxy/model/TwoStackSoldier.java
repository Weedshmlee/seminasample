package com.weedscomm.designpattern.structural.proxy.model;

import android.util.Log;

/**
 * Created by hmlee on 2018-02-26.
 */

public class TwoStackSoldier extends Soldier {
    private final String TAG = "일병";

    public TwoStackSoldier() {
        // 일병은 빠르게 준비를 마칩니다
        Log.v(TAG, "새로운 일병이 선택받았습니다");
    }

    @Override
    protected void listening() {
        working();
    }

    @Override
    public void working() {
        Log.v(TAG, "일병은 노오오오오오오력을 합니다");
    }
}
