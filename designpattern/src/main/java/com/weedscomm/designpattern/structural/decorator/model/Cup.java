package com.weedscomm.designpattern.structural.decorator.model;

import com.weedscomm.designpattern.structural.decorator.interfaces.ICoffee;

/**
 * Created by leewp on 18-02-26.
 */

public class Cup extends Additive {
    public Cup() {
        super(new ICoffee() {
            @Override
            public String getName() {
                return " 커피";
            }
        });
    }

    @Override
    public String getName() {
        return coffee.getName();
    }
}
