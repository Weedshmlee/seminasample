package com.weedscomm.designpattern.structural.proxy.model;

import android.util.Log;

import com.weedscomm.designpattern.structural.proxy.interfaces.ICaptain;

/**
 * Created by hmlee on 2018-02-26.
 */

public class Captain implements ICaptain {
    private final String TAG = "중대장";
    private OnCaller onCall;

    public Captain() {
        // 중대장은 당직사관에게 역할을 위임합니다
        onCall = new OnCaller();
    }

    @Override
    public void listen(String str) {
        Log.v(TAG, "중대장은 \"" + str + "\" 라는 말을 들었다!\n" +
                "프로 실망러인 중대장은 일단 실망한다");
        onCall.listen(str);
    }

    @Override
    public void order(String str) {
        Log.v(TAG, "중대장은 \"" + str + "\" 라고 소리쳤다");
        onCall.order(str);
    }
}
