package com.weedscomm.designpattern.structural.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weedscomm.designpattern.R;
import com.weedscomm.designpattern.structural.adapter.holder.AdapterHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hmlee on 2018-02-26.
 */

public class AdapterPattern extends RecyclerView.Adapter {
    private List<String> list;

    public AdapterPattern() {
        list = new ArrayList<>();
    }

    public void add(String str) {
        list.add(str);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new AdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((AdapterHolder) holder).getTextView().setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
