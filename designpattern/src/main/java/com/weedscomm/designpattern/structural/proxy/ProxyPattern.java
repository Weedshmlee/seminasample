package com.weedscomm.designpattern.structural.proxy;

import com.weedscomm.designpattern.structural.proxy.model.Captain;

/**
 * Created by hmlee on 2018-02-26.
 */

public class ProxyPattern {
    public void run() {
        // 대대장이 부대가 엉망인걸 보았다
        Captain captain = new Captain();
        captain.listen("중대장아 부대가 엉망이구나");
        captain.order("중대장은 너희들에게 실망했다");

        captain.listen("중대장아 부대정리 했니");
        captain.order("중대장은 너희들에게 실망했다");

        captain.listen("중대장아 부대가 깔끔하구나");
        captain.order("중대장은 너희들에게 실망했다");
    }
}
