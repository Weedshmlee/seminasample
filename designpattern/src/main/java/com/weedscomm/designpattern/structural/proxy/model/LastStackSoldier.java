package com.weedscomm.designpattern.structural.proxy.model;

import android.util.Log;

/**
 * Created by hmlee on 2018-02-26.
 */

public class LastStackSoldier extends Soldier {
    private final String TAG = "병장";

    public LastStackSoldier() {
        // 병장은 준비하지 않습니다
        Log.v(TAG, "새로운 병장이 선택받았습니다");
    }

    @Override
    protected void listening() {
        working();
    }

    @Override
    public void working() {
        Log.v(TAG, "병장은 사라졌습니다");
    }
}
