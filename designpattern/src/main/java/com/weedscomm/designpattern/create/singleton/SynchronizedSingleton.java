package com.weedscomm.designpattern.create.singleton;

/**
 * getInstance 시 synchronized 를 추가함으로 객체가 완전히 생성될 때 까지 접근을 방지한다
 * Thread-Safe 하지만 synchronized 는 성능 저하의 원인이 된다
 *
 * Created by hmlee on 2018-02-22.
 */

public class SynchronizedSingleton {
    private static SynchronizedSingleton ourInstance;

    public static synchronized SynchronizedSingleton getInstance() {
        if(null == ourInstance) {
            ourInstance = new SynchronizedSingleton();
        }

        return ourInstance;
    }

    private SynchronizedSingleton() {

    }
}
