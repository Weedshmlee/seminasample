package com.weedscomm.designpattern.create.factory.factories;

import com.weedscomm.designpattern.create.builder.BuilderPatternPizza;
import com.weedscomm.designpattern.create.factory.interfaces.IPizzaCreate;
import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Created by leewp on 18-02-25.
 */

public class MrPizzaFactory extends AbstractPizzaFactory implements IPizzaCreate {
    @Override
    public Pizza mainPizza() {
        return new BuilderPatternPizza.Builder()
                .setDough("얇은 도우")
                .setSouce("토마토 소스")
                .setTopping("치즈 토핑")
                .build();
    }

    @Override
    public Pizza subPizza() {
        return new BuilderPatternPizza.Builder()
                .setDough("두꺼운 도우")
                .setSouce("토마토 소스")
                .setTopping("불고기 토핑")
                .build();
    }

    @Override
    protected Pizza getMainPizza() {
        return mainPizza();
    }

    @Override
    protected Pizza getSubPizza() {
        return subPizza();
    }

    @Override
    protected Pizza getCheesePizza() {
        return new BuilderPatternPizza.Builder()
                .setDough("두꺼운 도우")
                .setSouce("토마토 소스")
                .setTopping("치즈 토핑")
                .build();
    }
}
