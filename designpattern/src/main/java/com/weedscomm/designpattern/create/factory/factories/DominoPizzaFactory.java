package com.weedscomm.designpattern.create.factory.factories;

import com.weedscomm.designpattern.create.builder.BuilderPatternPizza;
import com.weedscomm.designpattern.create.factory.interfaces.IPizzaCreate;
import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Created by leewp on 18-02-25.
 */

public class DominoPizzaFactory extends AbstractPizzaFactory implements IPizzaCreate {

    public Pizza getPizza(Pizza.PIZZA_KIND select) {
        switch (select) {
            case MAIN_PIZZA: return mainPizza();
            case SUB_PIZZA: return subPizza();
            case CHEESE_PIZZA: return cheesePizza();
            default: break;
        }

        return new BuilderPatternPizza.Builder().build();
    }

    @Override
    public Pizza mainPizza() {
        return new BuilderPatternPizza.Builder()
                .setDough("중간 도우")
                .setSouce("칠리 소스")
                .setTopping("치즈 토핑")
                .build();
    }

    @Override
    public Pizza subPizza() {
        return new BuilderPatternPizza.Builder()
                .setDough("중간 도우")
                .setSouce("칠리 소스")
                .setTopping("파인애플 토핑")
                .build();
    }

    private Pizza cheesePizza() {
        return new BuilderPatternPizza.Builder()
                .setDough("두꺼운 도우")
                .setSouce("칠리 소스")
                .setTopping("치즈 토핑")
                .build();
    }

    @Override
    protected Pizza getMainPizza() {
        return mainPizza();
    }

    @Override
    protected Pizza getSubPizza() {
        return subPizza();
    }

    @Override
    protected Pizza getCheesePizza() {
        return cheesePizza();
    }
}
