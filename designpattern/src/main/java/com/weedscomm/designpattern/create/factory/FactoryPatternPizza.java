package com.weedscomm.designpattern.create.factory;

import com.weedscomm.designpattern.create.factory.factories.DominoPizzaFactory;
import com.weedscomm.designpattern.create.factory.factories.PizzaOrderFactory;
import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Factory Pattern 에는 Factory Method Pattern (팩토리 메소드 패턴) 과 Abstract Factory Pattern (추상 팩토리 패턴) 이 있다
 * <p>
 * Factory Method Pattern 은 객체를 생성하는 인터페이스를 정의하고 Sub Class 에서 구현하는데
 * 이 객체를 생성하는 메소드가 Factory Method 다
 * Factory 를 호출한 Class 에서는 Factory 내에서 어떤 객체가 어떻게 생성되는지 신경쓰지않고
 * 반환된 객체를 사용만 하면 된다
 * 새로운 Model (예제에서는 Pizza) 이나 Factory 가 추가되더라도 변경될 코드는 최소화된다
 * <p>
 * 유념해야할 것은 객체를 생성하는 Pattern 이 아닌 Sub Class 에서 어떤 객체를 생성할지를 결정하도록 하는 패턴이라는 점이다
 * (거의 차이는 없지만)
 * <p>
 * Abstract Factory Pattern 은 객체 구성부분을 추상화해서 여러 서브클래스가 상속받아 객체 구성을 통일하는 방법
 * <p>
 * Created by leewp on 18-02-25.
 */

public class FactoryPatternPizza {

    public void factoryMethodPattern() {

        // 피자를 결정하기만 하면 된다.
        Pizza dominoCheesePizza = new DominoPizzaFactory().getPizza(Pizza.PIZZA_KIND.CHEESE_PIZZA);
    }

    public void abstractFactoryPattern() {
        // 피자를 주문한다.
        // 피자 주문 시 공정은 전부 같다.
        // 불을 준비하고, 피자를 준비하고, 피자를 굽고, 피자가 잘 구워졌으면 피자를 반환한다
        Pizza order1 = PizzaOrderFactory.order(Pizza.BRAND.MR_PIZZA, Pizza.PIZZA_KIND.MAIN_PIZZA);

        Pizza order2 = PizzaOrderFactory.order(Pizza.BRAND.DOMINO_PIZZA, Pizza.PIZZA_KIND.SUB_PIZZA);
    }
}
