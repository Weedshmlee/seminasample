package com.weedscomm.designpattern.create.singleton;

/**
 * 현재 Android Studio 에서 Singleton Pattern 생성시 만들어지는 패턴
 * Thread-safe 하다는 장점이 있지만 Class 가 load 되는 시점에 instance 를 생성시키기 때문에 비효율적일 수 있다 (메모리 누수)
 *
 * Created by hmlee on 2018-02-22.
 */

public class EagerSingleton {
    private static final EagerSingleton ourInstance = new EagerSingleton();

    public static EagerSingleton getInstance() {
        return ourInstance;
    }

    private EagerSingleton() {
    }
}
