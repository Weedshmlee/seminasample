package com.weedscomm.designpattern.create.factory.interfaces;

import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Created by leewp on 18-02-25.
 */

public interface IPizzaCreate {
    Pizza mainPizza();
    Pizza subPizza();
}
