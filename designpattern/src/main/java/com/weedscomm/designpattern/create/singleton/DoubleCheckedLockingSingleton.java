package com.weedscomm.designpattern.create.singleton;

/**
 * Thread-safe + Lazy-initialization 기법의 Singleton
 * 첫번째 if 문에서 instance 가 null 인 경우 synchronized 블럭에 접근하고, 한번 더 instance 의 null 유무를 체크
 * 이후에는 instance 가 null 이 아니기 때문에 synchronized 블럭을 타지 않는다
 *
 * Thread-safe 하면서 Lazy-initialization 방식이기 때문에 메모리 누수 및 성능저하를 보완할 수 있다
 *
 * Created by hmlee on 2018-02-22.
 */

public class DoubleCheckedLockingSingleton {
    private static DoubleCheckedLockingSingleton ourInstance ;

    public static DoubleCheckedLockingSingleton getInstance() {
        if(null == ourInstance) {
            synchronized (DoubleCheckedLockingSingleton.class) {
                if(null == ourInstance) {
                    ourInstance = new DoubleCheckedLockingSingleton();
                }
            }
        }
        return ourInstance;
    }

    private DoubleCheckedLockingSingleton() {

    }
}
