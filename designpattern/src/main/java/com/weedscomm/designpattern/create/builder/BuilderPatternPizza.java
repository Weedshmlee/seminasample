package com.weedscomm.designpattern.create.builder;

import com.weedscomm.designpattern.create.model.Pizza;

import java.util.Locale;

/**
 * Created by leewp on 18-02-24.
 */

public class BuilderPatternPizza extends Pizza {
    public static class Builder {
        private BuilderPatternPizza mBuilderPatternPizza;

        public Builder() {
            this(null);
        }

        public Builder(BuilderPatternPizza builderPatternPizza) {
            if (null == builderPatternPizza) {
                mBuilderPatternPizza = new BuilderPatternPizza();
            } else {
                mBuilderPatternPizza = builderPatternPizza;
            }
        }

        public Builder setDough(String dough) {
            mBuilderPatternPizza.setDough(dough);
            return this;
        }

        public Builder setSouce(String souce) {
            mBuilderPatternPizza.setSouce(souce);
            return this;
        }

        public Builder setTopping(String topping) {
            mBuilderPatternPizza.setTopping(topping);
            return this;
        }

        public Builder setDrink(String drink) {
            mBuilderPatternPizza.setDrink(drink);
            return this;
        }

        private boolean isEmpty(String pizzaMember) {
            return null == pizzaMember || "".equals(pizzaMember);
        }

        private void setDefaultDough() {
            mBuilderPatternPizza.setDough("기본 도우");
        }

        private void setDefaultSouce() {
            mBuilderPatternPizza.setSouce("토마토 소스");
        }

        private void setDefaultTopping() {
            mBuilderPatternPizza.setTopping("기본 치즈 토핑");
        }

        public BuilderPatternPizza build() {
            if (isEmpty(mBuilderPatternPizza.getDough())) {
                setDefaultDough();
            }

            if (isEmpty(mBuilderPatternPizza.getSouce())) {
                setDefaultSouce();
            }

            if (isEmpty(mBuilderPatternPizza.getTopping())) {
                setDefaultTopping();
            }

            return mBuilderPatternPizza;
        }
    }

    public String printPizzaInfo() {
        String str = String.format(Locale.getDefault(), "%s 에 %s 를 바르고 %s 를 올린 피자",
                this.getDough(),
                this.getSouce(),
                this.getTopping());
        if (!"".equals(this.getDrink())) {
            str += " " + this.getDrink() + " 를 추가하였습니다";
        }

        return str;
    }
}
