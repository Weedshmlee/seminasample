package com.weedscomm.designpattern.create.builder;

import android.util.Log;

/**
 * Created by leewp on 18-02-24.
 */

public class PizzaMaker {
    private final String TAG = "Pizza Maker";

    public void run() {
        // Builder 를 사용하지 않는다면
        BuilderPatternPizza pizza = new BuilderPatternPizza();
        pizza.setDough("얇은 도우");
        pizza.setTopping("파인애플 토핑");

        // Builder 사용시
        BuilderPatternPizza pizzaBuilder1 = new BuilderPatternPizza.Builder()
                .setDough("두꺼운 도우")
                .setTopping("불고기 토핑")
                .build();

        BuilderPatternPizza pizzaBuilder2 = new BuilderPatternPizza.Builder()
                .setDrink("코카콜라")
                .build();

        Log.v(TAG, "1. " + pizza.printPizzaInfo() + "\n" +
                "2. " + pizzaBuilder1.printPizzaInfo() + "\n" +
                "3. " + pizzaBuilder2.printPizzaInfo());

    }
}
