package com.weedscomm.designpattern.create.factory.factories;

import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Created by leewp on 18-02-25.
 */

public class PizzaOrderFactory {

    public static Pizza order(Pizza.BRAND brand, Pizza.PIZZA_KIND pizzaKind) {
        PizzaMakingFactory factory = new PizzaMakingFactory();

        if (factory.checkPizza() == Pizza.PIZZA_STATE.IS_NOT_READY_PIZZA) {
            // step 1. 불이 준비되어 있지 않다면 불을 준비한다
            factory.readyToFire();
        }

        // step 2. 불이 준비되었으면 피자 종류에 따라 만들 피자를 결정한다
        // 브랜드에 따라 피자브랜드를 선택하는 것이 추상 팩토리 패턴으로 구현되어 있다
        factory.readyToPizza(brand,pizzaKind);

        // step 3. 피자를 조리한다
        factory.cooking();

        if (factory.checkPizza() == Pizza.PIZZA_STATE.COOKING_GOOD) {
            // step 4. 피자가 완성되었으면 피자를 반환한다
            return factory.presentPizza();
        } else {
            // step 4-1. 피자 완성이 실패하였다면 피자를 다시 만든다
            return order(brand, pizzaKind);
        }
    }
}
