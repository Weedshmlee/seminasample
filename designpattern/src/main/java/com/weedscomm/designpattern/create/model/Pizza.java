package com.weedscomm.designpattern.create.model;

/**
 * Created by leewp on 18-02-24.
 */

public class Pizza {
    private String dough = "";
    private String souce = "";
    private String topping = "";
    private String drink = "";

    public String getDough() {
        return dough;
    }

    public void setDough(String dough) {
        this.dough = dough;
    }

    public String getSouce() {
        return souce;
    }

    public void setSouce(String souce) {
        this.souce = souce;
    }

    public String getTopping() {
        return topping;
    }

    public void setTopping(String topping) {
        this.topping = topping;
    }

    public String getDrink() {
        return drink;
    }

    public void setDrink(String drink) {
        this.drink = drink;
    }

    // 피자 가게를 정의
    public enum  BRAND {
        MR_PIZZA,
        DOMINO_PIZZA
    }

    // 피자 종류를 정의
    public enum PIZZA_KIND {
        MAIN_PIZZA,
        SUB_PIZZA,
        CHEESE_PIZZA
    }

    // 피자의 상태를 정의
    public enum PIZZA_STATE {
        IS_NOT_READY_PIZZA,
        COOKING_GOOD,
        COOKING_FAIL
    }
}
