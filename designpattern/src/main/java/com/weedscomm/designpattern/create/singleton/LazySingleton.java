package com.weedscomm.designpattern.create.singleton;

/**
 * 가장 간단하게 작성 가능한 Singleton Pattern 이다
 * Singleton 의 특징을 가장 잘 보여준다고 생각된다
 *
 * Lazy-initialization (늦은 초기화) 방식이기 때문에 메모리 누수는 줄어들지만 Thread-Safe 하지 못하다
 *
 * ******************************************************
 * 예시)
 * public static LazySingleton getInstance() {
 *
 *     if(null == ourInstance) {                    ---- (1)
 *         ourInstance = new LazySingleton();    ---- (2)
 *     }
 *
 *     return ourInstance;
 * }
 *
 * Multi Thread 환경에서 서로 다른 Thread 가 동시에 Singleton.getInstance() 를 호출하면 어떻게 될까
 *
 * ThreadA 에서 (1) 번 Flow 에 접근하였고 instance 가 생성되기 전이기 때문에 (2) 번 Flow 에 접근할 것이다
 * ThreadB 에서 ThreadA 가 (2) 번 Flow 를 처리하기 전에 (1) 번 Flow 에 접근하였다면
 * 아직 instance 는 생성되기 전이기 때문에 ThreadB 에서도 (2) 번 Flow 에 접근할 것이다.
 *
 * 그 결과 2개의 객체가 생성된다.
 *
 * 이러한 문제를 회피하기 위해 Singleton Pattern 도 여러가지 방법이 있다
 * ******************************************************
 *
 *
 * Created by hmlee on 2018-02-22.
 */

public class LazySingleton {
    private static LazySingleton ourInstance;

    public static LazySingleton getInstance() {
        if(null == ourInstance) {
            ourInstance = new LazySingleton();
        }

        return ourInstance;
    }

    private LazySingleton() {

    }
}
