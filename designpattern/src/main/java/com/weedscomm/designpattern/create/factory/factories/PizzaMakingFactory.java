package com.weedscomm.designpattern.create.factory.factories;

import com.weedscomm.designpattern.create.builder.BuilderPatternPizza;
import com.weedscomm.designpattern.create.factory.interfaces.IPizzaCooking;
import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Created by leewp on 18-02-25.
 */

public class PizzaMakingFactory implements IPizzaCooking {
    private boolean isFireReady = false;
    private Pizza pizza;

    private BuilderPatternPizza.Builder builder;
    private Pizza.PIZZA_STATE state = Pizza.PIZZA_STATE.IS_NOT_READY_PIZZA;

    public void readyToFire() {
        isFireReady = true;
    }

    @Override
    public void readyToPizza(Pizza.BRAND brand, Pizza.PIZZA_KIND pizza) {
        builder = new BuilderPatternPizza.Builder();

        switch (brand) {
            case MR_PIZZA:
                setPizza(new MrPizzaFactory(), pizza);
                break;
            case DOMINO_PIZZA:
                setPizza(new DominoPizzaFactory(), pizza);
                break;
            default:
                break;
        }
    }

    private void setPizza(AbstractPizzaFactory pizza, Pizza.PIZZA_KIND kind) {
        switch (kind) {
            case MAIN_PIZZA:
                this.pizza = pizza.getMainPizza();
                break;
            case SUB_PIZZA:
                this.pizza = pizza.getSubPizza();
                break;
            case CHEESE_PIZZA:
                this.pizza = pizza.getCheesePizza();
                break;
            default:
                break;
        }
    }

    private boolean isCookingFail(Pizza pizza) {
        // ...

        return false;
    }

    @Override
    public void cooking() {
        if (isFireReady) {
            this.pizza = builder.build();
        } else {
            this.state = Pizza.PIZZA_STATE.IS_NOT_READY_PIZZA;
        }
    }

    @Override
    public Pizza.PIZZA_STATE checkPizza() {
        if (!isCookingFail(this.pizza)) {
            this.state = Pizza.PIZZA_STATE.COOKING_GOOD;
        } else {
            this.state = Pizza.PIZZA_STATE.COOKING_FAIL;
        }

        return this.state;
    }

    @Override
    public Pizza presentPizza() {
        return this.pizza;
    }
}
