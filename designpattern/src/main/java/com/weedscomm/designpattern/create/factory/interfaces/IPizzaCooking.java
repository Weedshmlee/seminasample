package com.weedscomm.designpattern.create.factory.interfaces;

import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Created by leewp on 18-02-25.
 */

public interface IPizzaCooking {
    void readyToFire(); // 불을 준비한다
    void readyToPizza(Pizza.BRAND brand,Pizza.PIZZA_KIND pizza); // 피자를 준비한다
    void cooking(); // 피자를 요리한다
    Pizza.PIZZA_STATE checkPizza(); // 피자를 확인한다
    Pizza presentPizza(); // 피자 객체를 반환한다
}
