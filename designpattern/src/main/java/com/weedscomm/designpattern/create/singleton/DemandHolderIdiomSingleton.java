package com.weedscomm.designpattern.create.singleton;

/**
 * 미국 메릴랜드 대학의 컴퓨터 과학 연구원인 Bill pugh 가
 * 기존의 Java Holder Pattern 이 가지고 있는 문제들을 해결하기 위해 제시한 Pattern
 * Lazy-initialization 방식이면서 (getInstance 호출시 Holder Class 를 Load 한다)
 * Class 의 Load 시점에 내부 Class 를 생성시킴으로 Thread-safe 하다
 *
 * 모든 Java 버전과 JVM 에서 사용 가능하며 현재까지 가장 많이 사용되는 방법으로 알려진 Pattern
 *
 * Created by hmlee on 2018-02-22.
 */

public class DemandHolderIdiomSingleton {
    private static class Holder {
        private static final DemandHolderIdiomSingleton ourInstance = new DemandHolderIdiomSingleton();
    }

    public static DemandHolderIdiomSingleton getInstance() {
        return Holder.ourInstance;
    }

    private DemandHolderIdiomSingleton() {
    }
}
