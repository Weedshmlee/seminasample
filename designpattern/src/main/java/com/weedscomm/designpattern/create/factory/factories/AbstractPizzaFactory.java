package com.weedscomm.designpattern.create.factory.factories;

import com.weedscomm.designpattern.create.model.Pizza;

/**
 * Created by hmlee on 2018-02-26.
 */

public abstract class AbstractPizzaFactory {

    abstract Pizza getMainPizza();
    abstract Pizza getSubPizza();
    abstract Pizza getCheesePizza();
}
