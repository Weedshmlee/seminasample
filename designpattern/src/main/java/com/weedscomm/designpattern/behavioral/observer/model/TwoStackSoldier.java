package com.weedscomm.designpattern.behavioral.observer.model;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.observer.interfaces.IObserver;
import com.weedscomm.designpattern.behavioral.observer.interfaces.ISubject;

/**
 * Created by hmlee on 2018-02-26.
 */

public class TwoStackSoldier implements IObserver {
    private final String TAG = "일병";

    private ISubject publisher;

    public TwoStackSoldier(ISubject publisher) {
        // 일병은 빠르게 준비를 마칩니다
        Log.v(TAG, "새로운 일병이 선택받았습니다");
        this.publisher = publisher;
        publisher.register(this);
    }

    @Override
    public void listen(String msg) {
        Log.v(TAG, "일병은 " + msg + " 라는 방송을 듣자마자 준비가 끝났습니다");
    }

    public void unregister() {
        publisher.unregister(this);
        Log.v(TAG, "일병에게 더이상 당직사관의 말은 닿지 않습니다");
    }
}
