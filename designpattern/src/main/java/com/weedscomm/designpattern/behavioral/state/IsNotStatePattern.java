package com.weedscomm.designpattern.behavioral.state;

import com.weedscomm.designpattern.behavioral.state.notstatepattern.model.NDeveloper;

/**
 * Created by hmlee on 2018-02-27.
 */

public class IsNotStatePattern {


    public void run() {
        NDeveloper developer = new NDeveloper();
        developer.work();
        developer.work();
        developer.work();
        developer.work();
        developer.rest();
        developer.rest();
    }
}
