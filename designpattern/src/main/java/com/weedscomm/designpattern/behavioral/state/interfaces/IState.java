package com.weedscomm.designpattern.behavioral.state.interfaces;

import com.weedscomm.designpattern.behavioral.state.model.Developer;

/**
 * Created by hmlee on 2018-02-27.
 */

public interface IState {
    void work(Developer developer);
    void rest(Developer developer);
    void haaaaaaaardWork(Developer developer);
    String getStateName();
}
