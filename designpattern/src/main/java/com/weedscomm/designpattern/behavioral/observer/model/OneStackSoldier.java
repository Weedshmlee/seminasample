package com.weedscomm.designpattern.behavioral.observer.model;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.observer.interfaces.IObserver;
import com.weedscomm.designpattern.behavioral.observer.interfaces.ISubject;

/**
 * Created by hmlee on 2018-02-26.
 */

public class OneStackSoldier implements IObserver {
    private final String TAG = "이병";

    private ISubject publisher;

    public OneStackSoldier(ISubject publisher) {
        // 이등병은 준비에 매우 오랜 시간이 걸립니다
        Log.v(TAG, "새로운 이병이 선택받았습니다");
        this.publisher = publisher;
        publisher.register(this);
    }

    @Override
    public void listen(String msg) {
        Log.v(TAG, "이병은 " + msg + " 라는 방송을 들었습니다\n" +
                "이병은 느리지만 준비를 해서 집합합니다");
    }

    public void unregister() {
        publisher.unregister(this);
        Log.v(TAG, "이병에게 더이상 당직사관의 말은 닿지 않습니다");
    }
}
