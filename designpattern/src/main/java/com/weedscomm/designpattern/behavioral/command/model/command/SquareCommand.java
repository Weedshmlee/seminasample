package com.weedscomm.designpattern.behavioral.command.model.command;

import com.weedscomm.designpattern.behavioral.command.interfaces.IReceiver;
import com.weedscomm.designpattern.behavioral.command.model.receiver.SquareReceiver;

/**
 * Created by hmlee on 2018-02-27.
 */

public class SquareCommand extends BaseCommand {
    private SquareReceiver receiver;

    public SquareCommand(SquareReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    IReceiver getReceiver() {
        return receiver;
    }
}
