package com.weedscomm.designpattern.behavioral.state.notstatepattern;

/**
 * Created by hmlee on 2018-02-27.
 */

public enum DeveloperState {
    GOOD,
    TIRED,
    BAD,
    DIE // 상태가 추가된다면
}
