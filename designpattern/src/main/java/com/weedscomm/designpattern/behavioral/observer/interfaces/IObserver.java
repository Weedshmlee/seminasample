package com.weedscomm.designpattern.behavioral.observer.interfaces;

/**
 * IObserver Pattern 의 수신자
 * Created by hmlee on 2018-02-27.
 */

public interface IObserver {
    void listen(String msg);
}
