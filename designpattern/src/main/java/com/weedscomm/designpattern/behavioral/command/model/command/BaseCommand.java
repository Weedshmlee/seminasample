package com.weedscomm.designpattern.behavioral.command.model.command;

import com.weedscomm.designpattern.behavioral.command.interfaces.ICommand;
import com.weedscomm.designpattern.behavioral.command.interfaces.IReceiver;

/**
 * Created by hmlee on 2018-02-27.
 */

public abstract class BaseCommand implements ICommand {
    private final String TAG = "Base Command";

    public void execute() {
        getReceiver().draw();
    }

    public void undo() {
        getReceiver().delete();
    }

    public void redo() {
        getReceiver().draw();
    }

    abstract IReceiver getReceiver();
}
