package com.weedscomm.designpattern.behavioral.observer.model;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.observer.interfaces.IObserver;
import com.weedscomm.designpattern.behavioral.observer.interfaces.ISubject;

/**
 * Created by hmlee on 2018-02-26.
 */

public class ThreeStackSoldier implements IObserver {
    private final String TAG = "상병";

    private ISubject publisher;

    public ThreeStackSoldier(ISubject publisher) {
        // 상병은 이미 준비되어 있습니다.
        Log.v(TAG, "새로운 상병이 선택받았습니다");
        this.publisher = publisher;
        publisher.register(this);
    }

    @Override
    public void listen(String msg) {
        Log.v(TAG, "상병은 " + msg + " 라는 방송을 듣고 준비를 합니다");
    }

    public void unregister() {
        publisher.unregister(this);
        Log.v(TAG, "상병에게 더이상 당직사관의 말은 닿지 않습니다");
    }
}
