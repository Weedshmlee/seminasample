package com.weedscomm.designpattern.behavioral.state;

import com.weedscomm.designpattern.behavioral.state.model.Developer;

/**
 * DeveloperState Pattern
 * Created by hmlee on 2018-02-27.
 */

public class StatePattern {
    public void run() {
        Developer developer = new Developer();
        developer.haaaaaaaardWork();
        developer.haaaaaaaardWork();
        developer.haaaaaaaardWork();
        developer.haaaaaaaardWork();
        developer.haaaaaaaardWork();
        developer.rest();
        developer.work();
    }
}
