package com.weedscomm.designpattern.behavioral.command.model.receiver;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.command.interfaces.IReceiver;

import java.util.Locale;

/**
 * Created by hmlee on 2018-02-27.
 */

public class CircleReceiver implements IReceiver {
    private final String TAG = "Circle Receiver";
    private int x;
    private int y;

    public CircleReceiver(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void draw() {
        Log.v(TAG, String.format(Locale.getDefault(), "원을 그립니다 [%d, %d]", x, y));
    }

    @Override
    public void delete() {
        Log.v(TAG, String.format(Locale.getDefault(), "원을 지웁니다 [%d, %d]", x, y));
    }
}
