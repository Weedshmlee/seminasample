package com.weedscomm.designpattern.behavioral.command.model.command;

import com.weedscomm.designpattern.behavioral.command.interfaces.IReceiver;
import com.weedscomm.designpattern.behavioral.command.model.receiver.CircleReceiver;

/**
 * Created by hmlee on 2018-02-27.
 */

public class CircleCommand extends BaseCommand {
    private CircleReceiver receiver;

    public CircleCommand(CircleReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    IReceiver getReceiver() {
        return receiver;
    }
}
