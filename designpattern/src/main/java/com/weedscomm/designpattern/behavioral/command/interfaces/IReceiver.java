package com.weedscomm.designpattern.behavioral.command.interfaces;

/**
 * Created by hmlee on 2018-02-27.
 */

public interface IReceiver {
    void draw();
    void delete();
}
