package com.weedscomm.designpattern.behavioral.state.notstatepattern.model;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.state.notstatepattern.DeveloperState;

/**
 * Created by hmlee on 2018-02-27.
 */

public class NDeveloper {
    private final String TAG = "개발자2";

    private DeveloperState state;

    public NDeveloper() {
        this.state = DeveloperState.GOOD;
        Log.v(TAG, "개발자가 출근하였습니다\n" +
                "상태 : " + this.state.name());
    }

    public void setState(DeveloperState state) {
        this.state = state;
        Log.v(TAG, "개발자의 상태가 변화하였습니다\n" +
                "상태 : " + this.state.name());
    }

    public void work() {
        Log.v(TAG, "일을 하였습니다");
        switch (state) {
            case GOOD:
                setState(DeveloperState.TIRED);
                break;
            case TIRED:
                setState(DeveloperState.BAD);
                break;
            case BAD:
                Log.v(TAG, "무리한 업무를 하였습니다. 강제로 휴식에 들어갑니다");
                rest();
                break;
            case DIE: // State 가 추가되면 모든 switch ~ case 문 (또는 if문) 에 상태를 추가해 주어야 함
                Log.v(TAG, "죽은 개발자는 일할 수 없습니다");
                break;
        }
    }

    public void rest() {
        Log.v(TAG, "휴식을 취하였습니다");
        switch (state) {
            case GOOD:
                setState(DeveloperState.GOOD);
                break;
            case TIRED:
                setState(DeveloperState.GOOD);
                break;
            case BAD:
                setState(DeveloperState.TIRED);
                break;
            case DIE: // State 가 추가되면 모든 switch ~ case 문 (또는 if문) 에 상태를 추가해 주어야 함
                Log.v(TAG, "죽은 개발자는 쉴 수 없습니다");
                break;
        }
    }

    // Developer 의 Action 이 추가된다면 함수를 만들고 그에 따른 상태 조건문이 추가됨
    public void haaaaaaaardWork() {
        Log.v(TAG, "업무를 강요받았습니다");
        switch (state) {
            case GOOD:
                setState(DeveloperState.TIRED);
                break;
            case TIRED:
                setState(DeveloperState.BAD);
                break;
            case BAD:
                setState(DeveloperState.DIE);
                break;
            case DIE:
                Log.v(TAG, "죽은 개발자는 더이상 일할 수 없습니다");
                break;
        }
    }
}
