package com.weedscomm.designpattern.behavioral.state.state;

import com.weedscomm.designpattern.behavioral.state.interfaces.IState;
import com.weedscomm.designpattern.behavioral.state.model.Developer;

/**
 * Created by hmlee on 2018-02-27.
 */

public class StateTired implements IState {
    private final String TAG = "피곤함";

    // 불필요한 DeveloperState 객체가 생성되어 메모리 낭비를 방지하기 위해 Singleton 형태로 구현
    private static class Holder {
        private static final StateTired ourInstance = new StateTired();
    }

    public static StateTired getInstance() {
        return Holder.ourInstance;
    }

    private StateTired() {

    }

    @Override
    public void work(Developer developer) {
        developer.setState(StateBad.getInstance());
    }

    @Override
    public void rest(Developer developer) {
        developer.setState(StateGood.getInstance());
    }

    // Developer 동작이 추가되면 Interface 가 변경되었으므로 모든 상태에 함수를 추가해 주어야 함
    @Override
    public void haaaaaaaardWork(Developer developer) {
        developer.setState(StateBad.getInstance());
    }

    @Override
    public String getStateName() {
        return TAG;
    }
}
