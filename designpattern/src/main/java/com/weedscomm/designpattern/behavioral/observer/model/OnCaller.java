package com.weedscomm.designpattern.behavioral.observer.model;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.observer.interfaces.IObserver;
import com.weedscomm.designpattern.behavioral.observer.interfaces.ISubject;

import java.util.ArrayList;

/**
 * Created by hmlee on 2018-02-27.
 */

public class OnCaller implements ISubject {
    // 당직사관은 주체로 병사들에게 전달사항을 전달한다
    private final String TAG = "당직사관";

    private ArrayList<IObserver> observers;
    private String message;

    public OnCaller() {
        observers = new ArrayList<>();
    }

    @Override
    public void register(IObserver iObserver) {
        observers.add(iObserver);

    }

    @Override
    public void unregister(IObserver iObserver) {
        int index = observers.indexOf(iObserver);
        observers.remove(index);

    }

    @Override
    public void notification() {
        for (IObserver observer : (ArrayList<IObserver>)observers.clone()) {
            observer.listen(message);
        }
    }

    public synchronized void setMessage(String msg) {
        Log.v(TAG, "" + msg);
        message = msg;
        notification();
    }
}
