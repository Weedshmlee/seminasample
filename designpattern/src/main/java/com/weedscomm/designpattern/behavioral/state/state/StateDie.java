package com.weedscomm.designpattern.behavioral.state.state;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.state.interfaces.IState;
import com.weedscomm.designpattern.behavioral.state.model.Developer;

/**
 * 새로운 상태가 추가되어 State Class 추가
 * Created by hmlee on 2018-02-27.
 */

public class StateDie implements IState {
    private final String TAG = "죽음";

    // 불필요한 DeveloperState 객체가 생성되어 메모리 낭비를 방지하기 위해 Singleton 형태로 구현
    private static class Holder {
        private static final StateDie ourInstance = new StateDie();
    }

    public static StateDie getInstance() {
        return Holder.ourInstance;
    }

    private StateDie() {

    }

    @Override
    public void work(Developer developer) {
        Log.v(TAG, "죽은 개발자는 더이상 일할 수 없습니다");
    }

    @Override
    public void rest(Developer developer) {
        Log.v(TAG, "죽은 개발자는 쉴 수 없습니다");
    }

    // Developer 동작이 추가되면 Interface 가 변경되었으므로 모든 상태에 함수를 추가해 주어야 함
    @Override
    public void haaaaaaaardWork(Developer developer) {
        Log.v(TAG, "죽은 개발자는 더이상 일할 수 없습니다");
    }

    @Override
    public String getStateName() {
        return TAG;
    }
}
