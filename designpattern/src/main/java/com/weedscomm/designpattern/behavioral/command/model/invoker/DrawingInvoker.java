package com.weedscomm.designpattern.behavioral.command.model.invoker;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.command.interfaces.ICommand;

import java.util.Stack;

/**
 * Created by hmlee on 2018-02-27.
 */

public class DrawingInvoker {
    private final String TAG = "Invoker";

    private Stack<ICommand> history = new Stack<>();
    private Stack<ICommand> redoHistory = new Stack<>();

    public DrawingInvoker() {

    }

    public void execute(ICommand command) {
        Log.v(TAG, "execute");
        history.push(command);
        command.execute();
    }

    public void undo() {
        Log.v(TAG, "undo");
        if(history.size() > 0) {
            ICommand command = history.pop();
            command.undo();
            redoHistory.push(command);
        } else {
            Log.v(TAG, "더이상 취소할 작업이 없습니다");
        }
    }

    public void redo() {
        Log.v(TAG, "redo");
        if(redoHistory.size() > 0) {
            ICommand command = redoHistory.pop();
            command.redo();
            history.push(command);
        } else {
            Log.v(TAG, "더이상 복구할 작업이 없습니다");
        }
    }
}
