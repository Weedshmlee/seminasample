package com.weedscomm.designpattern.behavioral.state.state;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.state.interfaces.IState;
import com.weedscomm.designpattern.behavioral.state.model.Developer;

/**
 * Created by hmlee on 2018-02-27.
 */

public class StateBad implements IState {
    private final String TAG = "나쁨";

    // 불필요한 DeveloperState 객체가 생성되어 메모리 낭비를 방지하기 위해 Singleton 형태로 구현
    private static class Holder {
        private static final StateBad ourInstance = new StateBad();
    }

    public static StateBad getInstance() {
        return Holder.ourInstance;
    }

    private StateBad() {

    }

    @Override
    public void work(Developer developer) {
        Log.v(TAG, "무리한 업무를 하였습니다. 강제로 휴식에 들어갑니다");
        developer.rest();
    }

    @Override
    public void rest(Developer developer) {
        developer.setState(StateTired.getInstance());
    }

    // Developer 동작이 추가되면 Interface 가 변경되었으므로 모든 상태에 함수를 추가해 주어야 함
    @Override
    public void haaaaaaaardWork(Developer developer) {
        developer.setState(StateDie.getInstance());
    }

    @Override
    public String getStateName() {
        return TAG;
    }
}
