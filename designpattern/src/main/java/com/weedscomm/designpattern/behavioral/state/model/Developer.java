package com.weedscomm.designpattern.behavioral.state.model;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.state.interfaces.IState;
import com.weedscomm.designpattern.behavioral.state.state.StateGood;

/**
 * Created by hmlee on 2018-02-27.
 */

public class Developer {
    private final String TAG = "개발자";
    private IState state;

    public Developer() {
        // 개발자의 초기상태는 좋음 입니다
        this.state = StateGood.getInstance();
        Log.v(TAG, "개발자가 출근하였습니다.\n" +
                "상태 : " + this.state.getStateName());
    }

    public void setState(IState state) {
        this.state = state;
        Log.v(TAG, "개발자의 상태가 변화하였습니다\n" +
                "상태 : " + this.state.getStateName());
    }

    public void work() {
        Log.v(TAG, "일을 하였습니다");
        state.work(this);
    }

    public void rest() {
        Log.v(TAG, "휴식을 취하였습니다");
        state.rest(this);
    }

    // Developer 동작이 추가되면 Interface 가 변경되었으므로 함수를 추가해 주어야 함
    public void haaaaaaaardWork() {
        Log.v(TAG, "업무를 강요받았습니다");
        state.haaaaaaaardWork(this);
    }
}
