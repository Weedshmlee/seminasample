package com.weedscomm.designpattern.behavioral.command.interfaces;

/**
 * Created by hmlee on 2018-02-27.
 */

public interface ICommand {
    void execute();
    void undo();
    void redo();
}
