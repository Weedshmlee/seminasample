package com.weedscomm.designpattern.behavioral.observer.model;

import android.util.Log;

import com.weedscomm.designpattern.behavioral.observer.interfaces.IObserver;
import com.weedscomm.designpattern.behavioral.observer.interfaces.ISubject;

/**
 * Created by hmlee on 2018-02-26.
 */

public class LastStackSoldier implements IObserver {
    private final String TAG = "병장";

    private ISubject publisher;

    public LastStackSoldier(ISubject publisher) {
        // 병장은 준비하지 않습니다
        Log.v(TAG, "새로운 병장이 선택받았습니다");
        this.publisher = publisher;
        publisher.register(this);
    }

    @Override
    public void listen(String msg) {
        Log.v(TAG, "병장은 " + msg + " 라는 방송을 들었습니다\n" +
                "병장은 도망칩니다");
        unregister();
    }

    public void unregister() {
        publisher.unregister(this);
        Log.v(TAG, "병장에게 더이상 당직사관의 말은 닿지 않습니다");
    }
}
