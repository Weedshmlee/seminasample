package com.weedscomm.designpattern.behavioral.observer;

import com.weedscomm.designpattern.behavioral.observer.model.LastStackSoldier;
import com.weedscomm.designpattern.behavioral.observer.model.OnCaller;
import com.weedscomm.designpattern.behavioral.observer.model.OneStackSoldier;
import com.weedscomm.designpattern.behavioral.observer.model.ThreeStackSoldier;
import com.weedscomm.designpattern.behavioral.observer.model.TwoStackSoldier;

/**
 * Observer Pattern
 * Created by hmlee on 2018-02-27.
 */

public class ObserverPattern {
    private final String TAG = "ObserverPattern";

    public void run() {
        OnCaller onCaller = new OnCaller(); // 당직사관 생성

        onCaller.setMessage("밤새 눈이 많이 왔군!");

        // 당직사관의 말을 듣는 4명의 병사들이 있습니다
        LastStackSoldier soldier1 = new LastStackSoldier(onCaller);
        ThreeStackSoldier soldier2 = new ThreeStackSoldier(onCaller);
        TwoStackSoldier soldier3 = new TwoStackSoldier(onCaller);
        OneStackSoldier soldier4 = new OneStackSoldier(onCaller);

        onCaller.setMessage("당직사관이 전파한다. 주말이라 미안한데 밤새 눈이 와서 제설을 해야한다.\n" +
                "아침 점호는 제설준비해서 연병장으로 집합! 이상");

        onCaller.setMessage("다 모였나");

        onCaller.setMessage("김상병아 박병장좀 데리러 갔다와라");
        soldier2.unregister(); // 김상병은 박병장을 찾기위해 떠났다

        onCaller.setMessage("우리는 제설하러간다");
    }
}
