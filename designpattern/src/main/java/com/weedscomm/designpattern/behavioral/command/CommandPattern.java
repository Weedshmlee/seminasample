package com.weedscomm.designpattern.behavioral.command;

import com.weedscomm.designpattern.behavioral.command.model.command.CircleCommand;
import com.weedscomm.designpattern.behavioral.command.model.command.SquareCommand;
import com.weedscomm.designpattern.behavioral.command.model.invoker.DrawingInvoker;
import com.weedscomm.designpattern.behavioral.command.model.receiver.CircleReceiver;
import com.weedscomm.designpattern.behavioral.command.model.receiver.SquareReceiver;

/**
 * Command Pattern
 *
 * Created by hmlee on 2018-02-27.
 */

public class CommandPattern {
    public void run() {
        DrawingInvoker invoker = new DrawingInvoker();

        // 5, 5 원을 그린다
        invoker.execute(new CircleCommand(new CircleReceiver(5, 5)));
        // 5, 3 원을 그린다
        invoker.execute(new CircleCommand(new CircleReceiver(5, 3)));
        // 5, 5 사각형을 그린다
        invoker.execute(new SquareCommand(new SquareReceiver(5, 5)));
        // 취소
        invoker.undo();
        invoker.undo();
        // 복원
        invoker.redo();
        // 3, 3 원을 그린다
        invoker.execute(new CircleCommand(new CircleReceiver(3, 3)));
        // 취소
        invoker.undo();
        invoker.undo();
        invoker.undo();
        invoker.undo();
        invoker.undo();
    }
}
