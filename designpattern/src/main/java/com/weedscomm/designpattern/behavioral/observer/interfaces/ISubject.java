package com.weedscomm.designpattern.behavioral.observer.interfaces;

/**
 * IObserver Pattern 의 주제 객체
 * Created by hmlee on 2018-02-27.
 */

public interface ISubject {
    void register(IObserver iObserver);
    void unregister(IObserver iObserver);
    void notification();
}
